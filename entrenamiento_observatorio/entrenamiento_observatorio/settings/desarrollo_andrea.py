from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'entrenamiento_observatorio',
        'USER': 'entrenamiento_observatorio',
        'PASSWORD': 'entrenamiento_observatorio',
        'HOST': 'localhost',
        'ATOMIC_REQUEST': True,
    }
}

#Installed Apps
#INSTALLED_APPS += ['ejemplo_app',]
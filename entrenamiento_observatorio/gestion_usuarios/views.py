from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import CreateView, UpdateView, ListView, View, DetailView, TemplateView
from django.views.generic.edit import ModelFormMixin
from django.contrib import messages
from .models import Administrador
from .forms import  AdministradorCreateForm, AdministradorUpdateForm


# Create your views here.
class UsuarioPasswordMixin(object):

    def form_valid(self, form):
        form.instance.set_password(form.instance.password)
        messages.success(self.request, self.mensaje_exitoso)
        return super(UsuarioPasswordMixin, self).form_valid(form)

class UsuarioCreateView(UsuarioPasswordMixin, CreateView):
    pass


class UsuarioUpdateView(UsuarioPasswordMixin, UpdateView):

    def get_context_data(self, **kwargs):
        context = super(UsuarioUpdateView, self).get_context_data(**kwargs)
        context['edicion'] = True
        return context


class UsuarioListView(ListView):
    pass


class UsuarioDetailView(DetailView):
    pass


class UsuarioCambioEstadoMixin(ModelFormMixin):

   def get(self, *args, **kwargs):
       usuario = get_object_or_404(self.model, pk=kwargs['pk'])
       usuario.is_active = not usuario.is_active
       usuario.save()
       return redirect(usuario.get_absolute_url())


class ConfiguracionInicial(View):

    def get(self, *args, **kwargs):
        admins = Administrador.objects.all()
        print(admins)
        if len(admins) >= 0:
            messages.info(self.request, 'Por favor cree el administrador.')
            return redirect('gestion_usuarios:administrador:registrar')
        else:
            messages.warning(self.request, 'Ya existe un administrador.')
            return redirect('inicio')


class AdministradorCreateView(UsuarioCreateView):
    """
    Abril 8 de 2015
    Autor: Carolina Guzmán

    Crea el usuario administrador
    Hereda de la clase general UsuarioCreateView
    A partir de POST se obtiene la información para crear un usuario administrador y se almacena en la base de datos.
    Si el usuario se crea exitosamente se envia un mensaje tipo success y se redirecciona a la página de inicio.

    :param request: Petición Realizada
    :type request:    WSGIRequest
    """
    model = Administrador
    form_class = AdministradorCreateForm
    template_name = "gestion_usuarios/usuario_form.html"
    mensaje_exitoso = "Administrador creado exitosamente!"

    def get_success_url(self):
        return reverse('login')

    def get(self, *args, **kwargs):
        admins = Administrador.objects.all()
        if len(admins) > 2:
            messages.warning(self.request, 'Ya existe un administrador.')
            return redirect('login')
        else:
            return super(AdministradorCreateView, self).get(*args, **kwargs)


class AdministradorDetailView(UsuarioDetailView):
    model = Administrador


class AdministradorUpdateView(UsuarioUpdateView):
    model = Administrador
    form_class = AdministradorUpdateForm
    template_name = "gestion_usuarios/usuario_form.html"
    mensaje_exitoso = "Administrador actualizado exitosamente!"


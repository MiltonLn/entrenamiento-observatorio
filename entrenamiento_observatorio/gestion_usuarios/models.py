from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Create your models here.
class Usuario(User):
    TIPO_IDENTIFICACION = (
        (0, 'CÉDULA DE CIUDADANÍA'),
        (1, 'CÓDIGO DE ESTUDIANTE'),
    )

    tipo_id = models.IntegerField(verbose_name='Tipo de identificación', choices=TIPO_IDENTIFICACION)
    identificacion = models.CharField(max_length=50, verbose_name='Número de identificación', unique=True)
    fecha_nacimiento = models.DateField(verbose_name='Fecha de nacimiento')
    foto = models.ImageField(upload_to='fotos_usuario/')

    class Meta:
        abstract = True



class Administrador(Usuario):

    def get_absolute_url(self):
        return reverse('gestion_usuarios:administrador:detalles',kwargs={'pk': self.id})

    class Meta:
        permissions = (
            ('list_administrador', 'Can list administrador'),
        )

PERMISOS_ADMINISTRADOR = [
    "add_profesor",
    "add_estudiante",
    "change_administrador",
    "change_profesor",
    "change_estudiante",
    "delete_profesor",
    "delete_estudiante",
]
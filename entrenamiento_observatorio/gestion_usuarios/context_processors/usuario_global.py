from gestion_usuarios.models import Administrador


def obtener_objeto_aux(modelos, user_id):
    for modelo in modelos:
        try:
            objeto_usuario = modelo.objects.get(id=user_id)
            return objeto_usuario
        except modelo.DoesNotExist:
            continue


def usuario_global(request):
    if hasattr(request, 'user'):
        objeto_usuario = obtener_objeto_aux(
            [Administrador],
            request.user.id
        )
        return {'usuario': objeto_usuario}
    else:
        return {}
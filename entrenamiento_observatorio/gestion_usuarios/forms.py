from django import forms
from .models import Administrador
from entrenamiento_observatorio.utilities import MyDateWidget

class UsuarioCreateForm(forms.ModelForm):
    confirmar_pass = forms.CharField(required = False, widget = forms.PasswordInput(), label = "Confirmar contraseña")

    def __init__(self, *args, **kwargs):
        super(UsuarioCreateForm, self).__init__(*args, **kwargs)
        if self.instance.pk is None:
            self.fields['password'].widget = forms.PasswordInput()
        self.fields['fecha_nacimiento'].widget = MyDateWidget()

    def clean(self):
        cleaned_data = super(UsuarioCreateForm, self).clean()
        if self.instance.pk is None:
            password = cleaned_data.get('password')
            password2 = cleaned_data.get('confirmar_pass')

            if password != password2:
                msg_error = "Las contraseñas no coinciden"
                self.add_error('confirmar_pass', msg_error)
            return cleaned_data
        else:
            return cleaned_data

    def clean_identificacion(self):
        instance = getattr(self, 'instance',None)
        if instance and instance.pk:
            return instance.identificacion
        else:
            return self.cleaned_data['identificacion']

    def clean_tipo_id(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.tipo_id
        else:
            return self.cleaned_data['tipo_id']

    def clean_username(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.username
        else:
            return self.cleaned_data['username']


class AdministradorCreateForm(UsuarioCreateForm):

    class Meta():
        model = Administrador
        fields = ['username', 'first_name', 'last_name', 'password', 'tipo_id', 'identificacion', 'fecha_nacimiento',
                  'email', 'foto']

class AdministradorUpdateForm(AdministradorCreateForm):

    def __init__(self, *args, **kwargs):
        super(AdministradorUpdateForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = 1
        self.fields['tipo_id'].widget.attrs['readonly'] = 1
        self.fields['tipo_id'].widget.attrs.update({'style': 'pointer-events:none'})
        self.fields['identificacion'].widget.attrs['readonly'] = 1

    class Meta(AdministradorCreateForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'tipo_id', 'identificacion', 'fecha_nacimiento',
                  'email', 'foto']

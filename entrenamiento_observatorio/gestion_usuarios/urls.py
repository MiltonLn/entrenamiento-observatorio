from django.conf.urls import include, url
from django.views.generic import  TemplateView

from .views import ConfiguracionInicial, AdministradorCreateView, AdministradorDetailView, AdministradorUpdateView

extrapatters_administrador = [
    url(r'^inicial$', ConfiguracionInicial.as_view(), name='inicial'),
    url(r'^registrar$', AdministradorCreateView.as_view(), name='registrar'),
    url(r'^detalles/(?P<pk>\d+)/$', AdministradorDetailView.as_view(), name="detalles"),
    url(r'^actualizar/(?P<pk>\d+)/$', AdministradorUpdateView.as_view(), name="actualizar"),
]

"""extrapatters_estudiante = [
    url(r'^registrar-estudiante$', EstudianteCreateView.as_view(), name='registrar_estudiante'),
    url(r'^editar-estudiante/(?P<pk>\d+)/$', EstudianteUpdateView.as_view(), name='editar_estudiante'),
    url(r'^cambio-estado-estudiante/(?P<pk>\d+)/$', EstudianteCambioEstadoView.as_view(), name='cambio_estado_estudiante'),
    url(r'^listar-estudiante$', EstudianteListView.as_view(), name='listar_estudiante'),
]"""

urlpatterns = [
    url(r'^administrador/', include(extrapatters_administrador, namespace="administrador")),

]

"""
    url(r'^registrar-profesor$', ProfesorCreateView.as_view(), name='registrar_profesor'),
    url(r'^editar-profesor/(?P<pk>\d+)/$', ProfesorUpdateView.as_view(), name='editar_profesor'),
    url(r'^cambio-estado-profesor/(?P<pk>\d+)/$', ProfesorCambioEstadoView.as_view(), name='cambio_estado_profesor'),
    url(r'^listar-profesor$', ProfesorListView.as_view(), name='listar_profesor'),"""